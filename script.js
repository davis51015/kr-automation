//this is a function that fires when the webapp receives a GET request
function doGet(e) {
    return HtmlService.createHtmlOutput("request received");
  }
  
  
  //this is a function that fires when the webapp receives a POST request
  function doPost(e) {
    
    if (e !== undefined) {
      var myData             = JSON.parse([e.postData.contents]);
      
      var order_created      = myData.date_created;
      var order_number       = myData.number;
      var shipping_name      = myData.shipping.first_name + " " + myData.shipping.last_name;
      var shipping_address   = myData.shipping.address_1 + ", " + myData.shipping.city + " " + myData.shipping.state + ", " + myData.shipping.postcode;
      var shipping_address_2 = myData.shipping.address_2;
      var product_name       = myData.line_items[0].name;  //grab entire array
      var product_qty        = myData.line_items[0].quantity;
      var customer_note      = myData.customer_note;
      var order_total        = "$" + myData.total;
      var order_status       = myData.status;
      
     if(order_status == "completed") {
       
       
        var sheet = SpreadsheetApp.getActiveSheet();
        sheet.appendRow([order_created, order_number, shipping_name, shipping_address, shipping_address_2, product_name, product_qty, customer_note, order_total]);
      
        
        var i = 1;
        while (myData.line_items[i].name !== undefined){
          product_name       = myData.line_items[i].name;
          product_qty        = myData.line_items[i].quantity;
          sheet.appendRow([ "" ,"" ,"", "" ,"ADDITIONAL LINE ITEMS", product_name, product_qty,"" ,"" ]);
          
          Logger.log('Checking value for product name: ' + product_name);
          
          i++;
        }
        
       
      }
      
    }
  }